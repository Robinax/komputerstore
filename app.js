//Calling elements from html
const laptopsElement = document.getElementById("laptops");
const paymentleftElement = document.getElementById("paymentleft");
const getLoanElement = document.getElementById("getLoanBtn");
const payElement = document.getElementById("workpay");
const featureElemet = document.getElementById("laptopFeature");
const cardTitleElement = document.getElementById("laptopTitle");
const cardDescriptionElement = document.getElementById("laptopDescription");
const workBtnElemental = document.getElementById("workBtn");
const bankBtnElement = document.getElementById("bankBtn");
const laptopImgElement = document.getElementById("laptopImg");
const laptopPriceElement = document.getElementById("laptopPrice");
const repayLoanElement = document.getElementById("repayLoan");
const buyLaptopElement = document.getElementById("buyLaptopBtn");

//Global variables
let inBank = 0;
let paymentLeft = 0;
let holdingCash = 0;
let laptops = [];

//Fetching the computer API
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
  .then((response) => response.json())
  .then((data) => (laptops = data))
  .then((laptops) => addLaptopsToMenu(laptops));
//Adding the menu to the select element aswell writing out the first laptop from
//the API so there is something at first.
const addLaptopsToMenu = (laptops) => {
  laptops.forEach((x) => addLaptopToMenu(x));
  uppdatetext();
  laptopImgElement.src = `https://noroff-komputer-store-api.herokuapp.com/${laptops[0].image}`;
  laptopPriceElement.innerText = laptops[0].price;
  cardTitleElement.innerText = laptops[0].title;
  cardDescriptionElement.innerText = laptops[0].description;
  laptops[0].specs.forEach((x) => laptopspecs(x));
};
//Funtion to loop the specs so everything comes out as a list.
function laptopspecs(laptop){
  const featuresElemet = document.createElement("li");
  featuresElemet.innerText = laptop;
  featureElemet.appendChild(featuresElemet);
} 
//Same as above, function to create the option element for the select.
const addLaptopToMenu = (laptop) => {
  const laptopElement = document.createElement("option");
  laptopElement.value = laptop.id;
  laptopElement.appendChild(document.createTextNode(laptop.title));
  laptopsElement.appendChild(laptopElement);
};
//Function to check the loan, if there is not a loan hide the button/loans.
//If there is a loan show them.
function checkLoan() {
  if (paymentLeft > 0) {
    paymentleftElement.className = "loanLeftVisible";
    document.getElementById("paymentText").className = "loanLeftVisible";
    repayLoanElement.className = "loanLeftVisible";
  } else {
    paymentleftElement.className = "loanLeft";
    document.getElementById("paymentText").className = "loanLeft";
    repayLoanElement.className = "loanLeft";
  }
}
//Function to update text fields/innertext of elements
function uppdatetext() {
  document.getElementById("balance").innerText = inBank;
  payElement.innerText = holdingCash;
  paymentleftElement.innerText = paymentLeft;
}
//This function handle the card from the html(the last box with all the data). takes away the specs before looping
//it so it does not get more children on itself
const handleCard = (e) => {
  const selectedLaptop = laptops[e.target.selectedIndex];
  featureElemet.replaceChildren();
  selectedLaptop.specs.forEach((x) => laptopspecs(x));
  cardTitleElement.innerText = selectedLaptop.title;
  cardDescriptionElement.innerText = selectedLaptop.description;
  laptopPriceElement.innerText = selectedLaptop.price;
  if(selectedLaptop.id == 5){
    laptopImgElement.src = `https://noroff-komputer-store-api.herokuapp.com/assets/images/5.png`
  }else
  laptopImgElement.src = `https://noroff-komputer-store-api.herokuapp.com/${selectedLaptop.image}`;
};
//function for the bank button. Here it checks if there is any payments in the loan left
//and if there is 10% of the money will go to the loan and if not the money will
//go to the bank.
const handleBank = () => {
  if(holdingCash <=0){
    alert("You dont have any money")
  }else
  if (paymentLeft > 0) {
    if (paymentLeft <= holdingCash * 0.1) {
      const sumofpay = paymentLeft;
      paymentLeft -= holdingCash;
      holdingCash = holdingCash - sumofpay;
      inBank += holdingCash;
      holdingCash = 0;
      uppdatetext();
      checkLoan();
    } else {
      paymentLeft -= holdingCash * 0.1;
      holdingCash -= holdingCash * 0.1;
      inBank += holdingCash;
      holdingCash = 0;
      uppdatetext();
      checkLoan();
    }
    if (paymentLeft < 0) {
      inBank + paymentLeft;
      paymentLeft = 0;
      holdingCash = 0;
      uppdatetext();
    }
  } else {
    inBank += holdingCash;
    holdingCash = 0;
    uppdatetext();
    checkLoan();
  }
};
//This function handle the Loan button function. tried to do so the prompt only works with
//numbers and no letters.
const handleLoan = () => {
  const moneyinput = prompt(
    "How much do you want to loan? You can only take a loan 2 times your balance"
  );
  const intMoney = parseInt(moneyinput);
  if (moneyinput == null) {
    alert("Hey u didnt want a loan?");
  } else if (moneyinput.match(/^[0-9]+$/))
    if (paymentLeft > 0) {
      alert("You already have a loan! you need to pay that first");
    } else if (moneyinput < 0) {
      alert("What are you doing? Ofcourse this wont work..");
    } else if (moneyinput > inBank * 2) {
      alert("You cant take a loan this big!");
    } else {
      const totalCashLoaning = (holdingCash += intMoney);
      inBank += totalCashLoaning;
      paymentleftElement.innerText = moneyinput;
      paymentLeft = moneyinput;
      holdingCash = 0;
      uppdatetext();
      checkLoan();
    }
  else alert("wrong format");
};
//This function handle the button for repayloan
function repayloanhandler() {
  if(holdingCash <= 0){
    alert("You dont have any cast to pay the bank with!")
  }else
  if (holdingCash >= paymentLeft) {
    const cashsum = holdingCash;
    holdingCash -= paymentLeft;
    paymentLeft -= cashsum;
    checkLoan();
    uppdatetext();
  } else {
    paymentLeft -= holdingCash;
    holdingCash = 0;
    checkLoan();
    uppdatetext();
  }
}
//Function for the buy laptop were it checks if the balance inbank is greater than the
//price of the computer.
function buyLaptop() {
  const sumofcomputer = laptopPriceElement.textContent;
  if (inBank < sumofcomputer) {
    alert(
      "You cannot afford this computer, do some work or buy a cheaper one."
    );
  } else if (inBank >= sumofcomputer) {
    inBank -= sumofcomputer;
    uppdatetext();
    alert("You are now a proud new owner of: " + cardTitleElement.textContent);
  }
}

//Event listener for buttons and for the select so everything uppdates with change.
bankBtnElement.addEventListener("click", handleBank);
laptopsElement.addEventListener("change", handleCard);
getLoanElement.addEventListener("click", handleLoan);
buyLaptopElement.addEventListener("click", buyLaptop);
repayLoanElement.addEventListener("click", repayloanhandler);

//Eventlistener with arrowfunction for the workbtn. adding 100 to the cash in pay
workBtnElemental.addEventListener("click", () => {
  holdingCash += 100;
  payElement.innerText = holdingCash;
});